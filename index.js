


// TRAINER OBJECT
/*========================================================================================*/
let trainer = {
	name: 'Mash Ketchup',
	age: 22,
	pokemon: ['Trubbish','Mr. Mime','Jynx','Lickitung'],
	friends:{
		allies: ['Misty','Brock'],
		enemies: ['Jessy','James']
	},
	chooses: function(pokemonChosen){
		if (trainer.pokemon.includes(pokemonChosen) === true) {
			console.log(pokemonChosen+'! ' + trainer.name + ' chooses you!');
			return pokemonChosen; 
		} else {
			console.log(pokemonChosen + " is not one of your Pokemons!");
			alert(pokemonChosen + " is not one of your Pokemons!");
			return false;
		}
	}
}

/*========================================================================================*/


// TRAINER 'TALK OBJECT METHOD'
/*========================================================================================*/
	let chosenPokemon = trainer.chooses(prompt(trainer.name+", which Pokemon do you choose? \nYour Pokemons are: \n\nTrubbish (Level 23) \nMr. Mime (Level 25) \nJynx (Level 19) \nLickitung (Level 28)"));
/*========================================================================================*/





// POKEMON CONSTRUCTOR
/*========================================================================================*/
	function Pokemon(name, level, relationship) {

				    // Properties
				    this.name = name;
				    this.level = level;
				    this.health = 2 * level;
				    this.attack = 0.5 * level;
				    this.relationship = relationship;

				    //Methods
				 	
						this.faint = function(){
				    		console.log(this.name + ' fainted.');
				    		alert(this.name + ' fainted.')
				    		alert(trainer.name + ' loses.')
				    	}

				   

				    	this.tackle = function(target){
				    		console.log('');
				    		console.log (this.name +"'s starting health: "+this.health);
				    		console.log (target.name +"'s starting health: "+target.health);
				    		console.log('');

				    		for(this.health; this.health > 0; this.health){

				    			if(this.health > 0){
				    				console.log('========================================================');
				    				console.log('');
				    				console.log(this.name + ' tackled ' + target.name);
				    				target.health = target.health - this.attack;
				    				console.log(target.name+ " 's health is now reduced to " + target.health);
				    				console.log('');
				    			
				    			} 

				    			if(target.health > 0){
				    				this.health = this.health - target.attack;
				    				console.log(target.name+ " tackled back and reduced "+ this.name +"'s health to " + this.health);
				    				console.log('');
				    				console.log('========================================================');
				    			}

				    			if(this.health <= 0){
				    				console.log('========================================================');
				    				console.log('');
				    				this.faint();
				    				break;
				    			}

				    			if(target.health <= 0){
				    				if (target.relationship == 'enemy'){
				    					console.log('========================================================');
				    					console.log('');
				    					console.log(target.name + ' was KILLED.');
				    					console.log('YOU WIN!');
				    					alert(target.name + ' was KILLED.');
				    					alert('YOU WIN!');
				    				} else {
				    					console.log('========================================================');
				    					console.log('');
				    					console.log(target.name + ' fainted.');
				    					console.log('YOU WIN!');
				    					alert(target.name + ' fainted.');
				    					alert('YOU WIN!');
				    				}
				    				break;
				    			}
				    		}
				    	}
				    }
/*========================================================================================*/
				

				/*-----------Mash Ketchup's Pokemons----------*/

				let mrMime = new Pokemon('Mr. Mime', 25, 'player');
				let trubbish = new Pokemon('Trubbish', 23, 'player');
				let jynx = new Pokemon('Jynx', 19, 'player');
				let lickitung = new Pokemon('Lickitung', 38, 'player');
				/*--------------------------------------------*/

				/*---------------------Nemesis Pokemons---------------------*/
				let darkAlakazam = new Pokemon('Dark Alakazam', 24, 'enemy');
				let darkArbok = new Pokemon('Dark Arbok', 26, 'enemy');
				/*----------------------------------------------------------*/

				/*----------------------Ally Pokemons----------------------*/
				let blastoise = new Pokemon('Blastoise', 16, 'ally');
				let charizard = new Pokemon('Charizard', 21, 'ally');
				/*---------------------------------------------------------*/

				
/*========================================================================================*/

// TRAINER POKEMON IDS:
/*========================================================================================*/
	function returnPokemonID (chosenPokemon){
		if (chosenPokemon == 'Trubbish'){
			return 'trubbish';
		} 
		else if (chosenPokemon == 'Mr. Mime'){
			return 'mrMime';
		}
		else if (chosenPokemon == 'Jynx'){
			return 'jynx';
		}
		else if (chosenPokemon == 'Lickitung'){
			return 'lickitung';
		} 
	}

let pokemonID = returnPokemonID(chosenPokemon);

/*========================================================================================*/


// OPPONENT POKEMON IDS:
/*========================================================================================*/
	function returnOpponentPokemonID (opponentPokemon){
		if (opponentPokemon == 'Dark Alakazam'){
			return 'darkAlakazam';
		} 
		else if (opponentPokemon == 'Dark Arbok'){
			return 'darkArbok';
		}
		else if (opponentPokemon == 'Blastoise'){
			return 'blastoise';
		}
		else if (opponentPokemon == 'Charizard'){
			return 'charizard';
		} 
	}
/*========================================================================================*/

// BATTLE
/*========================================================================================*/
if (chosenPokemon != false) {
let opponentPokemon = prompt(trainer.name+", which Pokemon do you choose to battle? \nIf chosen opponent is from a FRIENDLY trainer, loss condition is faint. \nIf ENEMY, loss condition is DEATH. \n\nFriendly Opponents are: \nBlastoise (Level 16)\nCharizard (Level 21) \n\nFIGHT TO THE DEATH Opponents: \nDark Alakazam (Level 24) \nDark Arbok (Level 26)");
let opponentPokemonID = returnOpponentPokemonID(opponentPokemon);

	console.log('You have chosen to battle ' + opponentPokemon + ", who is an " + eval(opponentPokemonID).relationship);
	alert("THE BATTLE WILL NOW COMMENCE!");
	eval(pokemonID).tackle(eval(opponentPokemonID)); 
} else {
	alert('Please refresh the browser and choose a valid Pokemon.');
}
/*========================================================================================*/